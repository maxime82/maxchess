
def main():
    print("""
    
      ♞ Chess Tournament Scoreboard ♖
      
    [1] Players    ♙
    [2] Tournaments ♕
    [3] Reports     ♗
    [4] Exit ↵

    """)

# CREATE TOURNAMENT
def tournament():
    print("""
    
    ♙ Welcome to the tournaments' Menu ♙

    [1] Start New Tournament
    [2] Resume Tournament
    [3] Exit to Main Menu ↵

    """)

def tournament_players():
    print("""
        
    ♙ Create New Tournament ♙

    [1] Create New Players List
    [2] Use Existing Players List
    [3] Exit to Main Menu ↵

    
    
    """)

# CREATE PLAYERS
def player():
    print("""
    
    ♙ Welcome to the players' Menu ♙

    [1] Add Player
    [2] View Players
    [3] Delete Players
    [4] Exit to Main Menu ↵

    """)




# CREATE ROUNDS
def round():
    print("""
    
    ♙ Welcome to the First Round's Menu ♙

    [1] Start First Round
    [2] View Rounds
    [3] Delete Rounds 
    [4] Exit to Main Menu ↵

    """)

def rounds():
    print("""
    
    ♙ Welcome to the Rounds' Menu ♙

    [1] Start Next Round
    [3] Exit to Main Menu ↵

    """)

def rounds_results():
    print("""
    
    ♙ Welcome to the Rounds' Menu ♙

    [1] View all Rounds Results
    [3] Exit to Main Menu ↵

    """)


# CREATE REPORTS

def reports():
    print("""
    
    ♙ Welcome to the reports' Menu ♙

    [1] View Reports
    [3] Exit to Main Menu ↵

    """)
