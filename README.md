# Fetch Packages

First you have to install modules and packages used

```cmd
pip3 install -r requirements.txt
```


# Launch App

Open a terminal and cd yourself to the project's folder
then you can run the menu.py file with 

```cmd
cd maxchess
python3 app.py
```

# TODO

-Finalize the TUI with curses module

-Configure Tournament Class in model/tournament.py


# Philippe

Section ajoutée par Philippe
